#SecretSparrow
Instruction how to run the program

###Pre-requisite
- Redis Server version 3.2.3 or above
- MySQL Server (Can be from XAMPP or equivalent software package)
- composer dependency management
- Node.JS 
- npm
- PHP version 5.5 or above
- cordova

###Instruction Client Side

User have to install Cordova and Node.js. the Node js downlaod page can be found here: https://nodejs.org/en/download/ . User can choose to download Windows installer or Mac installer  from the website, after downloaded and installed the Node js, launch the Node js and a command prompt launcher will pop up. User then install the Cordova by typing : npm install -g cordova and wait for the program to install cordova. User can test the Cordova installation by typing: Cordova --version in the command line. Then, user can start installing the ionic framework by typing in the command line: npm install -g ionic and wait for the program to install ionic. 
While waiting for ionic to install, user can install the Android SDK from here: https://developer.android.com/studio/index.html, go to the bottom of the page with the title: Get just the command line tools

After downloaded and installed the Android SDK, go back to the Node js command line. And from terminal/command line, go to this directory → incognito\Secret-Sparrow-Frontend-side by:

1. type cd incognito\Secret-Sparrow-Frontend-side in the command line and hit Enter.

2. type ionic platform add android and hit Enter in the command line again to install the android platform. 

3. type cd platform/android and Enter, wait for while and and type cls. 

4. type ionic android --release , Enter and wait for the program to load.

5. type ionic build android --release and the program will take a while to load the program.

6. The command line should display something like this: 
   incognito\Secret-Sparrow-Frontend-side\something.apk
	

There are external package that you need to install for the SecretSparrow Project by typing in cd incognito\Secret-Sparrow-Frontend-side:

1. cordova plugin add cordova-plugin-inappbrowser

2. cordova plugin add cordova-plugin-image-picker

3. cordova plugin add cordova-plugin-file-transfer

4. bower install ng-cordova-oauth -S

The .apk should be displayed in the command line idf user successfully created the .apk file, there’s a video link provided step by step guiding user how to create an .apk file from the ionic framework project: https://www.youtube.com/watch?v=mXkpD0M6w9I
If user want to load the .apk file into their Android smartphone, user have to open the file explorer and go to the folder directory:

User can connect their Android smartphone via a USB to their desktop to transfer the .apk file. In order to transfer the SecretSparrow.apk into Android phone, make sure to enable allowing installation outside google play store from the phone setting first, before transferring the .apk file, then only install. After installing  then launch SecretSparrow.apk on the phone to test the application.

###Instruction Server Side

Here are the instruction to install or run the server side code:


1. Open terminal or command line and go to SecretSparrow-backend folder
2. Export the database dump (inside SecretSparrow-database folder) to the MySQL server
3. Edit the database configuration for MySQL in database.php (go to /SecretSparrow-backend/config)
    and Redis Server in .env file
4. Run `php artisan serve --port=8080` or it can be run by typing `php artisan serve` (it will run on port 8000)
    - `--port` is used to change the port number
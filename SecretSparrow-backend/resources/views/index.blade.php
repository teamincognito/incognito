<!DOCTYPE html>
<html>
    <head>
      
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

         <!-- Google font-->
		 
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

        <!-- Required Framework-->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />

        <!-- Font Icons-->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css" />

        <!-- Parallax.css-->        
        <link rel="stylesheet" type="text/css" href="assets/css/jarallax.css">

        <!-- OWL.css-->
        <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">

        <!-- Magnific.css-->
        <link rel="stylesheet" type="text/css" href="assets/css/magnific-popup.css">

        <!-- Custom Scroolbar -->
        <link rel="stylesheet" type="text/css" href="assets/css/jquery.mCustomScrollbar.css">

        <!--Animate CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.min.css">

        <!-- Wave-Effects.css-->
        <link rel="stylesheet" type="text/css" href="assets/css/waves.css">       

        <!-- Style.css-->   
        <link rel="stylesheet" type="text/css" href="assets/css/style.css" /> 

        <!-- Responsive.css-->   
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css" /> 
        <title>SecretSparrow</title>

        <!--color css-->
        <link rel="stylesheet" type="text/css" href="assets/css/color/color-1.css" id="color"/>
        <style>
            .spacing{
                margin-top: 20px;!important
            }
            .addthis_toolbox {
                display: -webkit-inline-box;
                display: -moz-inline-box;
                display: inline-flex;
            }
            .addthis_default_style .at300b {
                float: none;
            }
        </style>
        <!-- Google Tag Manager -->
        
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="50">
        <!-- <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NQ77TG"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NQ77TG');</script> -->
        <!-- End Google Tag Manager -->
    
        <!--Preloader-->                     
        <div id="btry-loader">
            <div class="btry inner">
                <div class="btry-charge"></div>
            </div>
        </div>
        <!--Nav Menu Starts-->
        <nav class="navbar default navbar-fixed-top">
            <div class="container mob-logo"> 
                <div class="navbar-header col-sm-2 tablet-logo">
                    <button type="button" class="navbar-toggle mob-menu" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>

                    </button>
                    <div class="mobile-sidebar">                                                  
                        <span class="sidebar-btn btn fa-bar-hide" data-action="toggle" data-side="right">
                            <i class="fa fa-bars"></i>
                        </span>                  
                    </div>                        
                    <a href="/" class="brand-logo">

                        <img src="assets/images/Secret%20Sparrow%20Title-total-white.png">

                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav pull-right">
                        <li class="active">
                            <a href="#home">Home</a>
                        </li>                      
                        <li>
                            <a href="#how_it_works">How it Works</a>
                        </li>
                        <li>
                            <a href="#features">Features</a>
                        </li>
                        <li>
                            <a href="#video">Video</a>
                        </li>
                        <li>
                            <a href="#screenshots">Screenshots</a>
                        </li>
                        <li>
                            <a href="#subscribe">Subscribe</a>
                        </li> 
                    </ul>                           
                </div>  

            </div>   
        </nav> 
       

        <!--Nav Menu Ends--> 

        <!-- Home Section Starts -->  

        <header id="home">        

            <section class="home_slider_bg" id="index-banner">

            <div class="bg-opacity-layer"></div>
                 
                <!--Cloud Section End--> 
                <div id="header" class="home-center">
                    <div class="container text-center">
                        <h1>Secret Sparrow</h1>
                        <p class="mt30">
                            A Crowdsourcing Platform <br>to gain more Twitter Followers
                        </p>
                        <div class="row text-center">
                            <div class="col-md-7 hidden-xs hidden-sm os-animation animated fadeInLeft" data-os-animation="fadeInLeft" data-os-animation-delay="0.5s" style="animation-delay: 0.5s;" data-os-animation-duration="2.5s">
                                <img class="img img-responsive" src="assets/images/home_mob.png" alt="Home-Mobile">
                            </div>
                            <div class="col-md-5">
                                <div class="btn-wrap">
                                    <a class="btn default_color available-btn colored-button float-button-light" href="#subscribe">
                                        <i class="fa fa-android"></i>
                                        <em>Join The </em>
                                        WAITING LIST
                                    </a>                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 
        </header>  
        <!-- Home Section Ends -->  
        <!-- how it works Starts -->
        <section id="how_it_works">
            <div class="container text-center">
                <h2 class="mb50">2 User Roles: <span class="bolder">Business Owner</span> and <span class="bolder">Crowdies</span></h2>
                <div style="width: 100%; display: table;">
                    <div style="display: table-row">
                       <!--  <div style="width: 600px; display: table-cell;">  -->
                        <div class="col-md-6 col-sm-12">
                            <i class="fa fa-briefcase fa-5x"></i>
                            <h3>Business Owner</h3>
                            <p>
                                Business company that is looking for <br>more Twitter followers
                            </p> 
                        </div>
                        <div class="col-md-6 col-sm-12"> 
                        <i class="fa fa-users fa-5x"></i>
                        <h3>Crowdies</h3>
                            <p>
                                Users helping the Businesses to grow
                            </p> 
                            </div>
                    </div>
                </div>
                
               
            </div>
        </section>
        <!-- Magical Features Ends -->
        <!-- Start [Features Description] -->
        <section id="feature-desc"> 
            <div class="container text-center">
            <h1 class="mb40">How it Works?</h1>
            <h2 class="mb40">Just <span class="bolder">3</span> simple steps</h2>            
                <div class="row">
                    <div class="col-md-4 col-sm-12 text-center">
                        <div class="icon-block mb50 mb_margin30">
                        <span class="icon-color">
                        <i class="fa fa-street-view" aria-hidden="true"></i>
                        </span>
                            <h3><strong>Step 1</strong></h3>
                            <p>
                                <span class="bolder">Business Owner's account follows high-profilers with common interests</span><br>
                                High-profilers are people who are famous and has many followers
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 text-center">
                        <div class="icon-block mb50 mb_margin30">
                            <span class="icon-color">
                                <i class="fa fa-twitter"></i>
                            </span>
                            <h3><strong>Step 2</strong></h3>
                            <p>
                                <span class="bolder">Crowdies work for your company by following suggested Twitter accounts</span><br>
                                These suggested Twitter accounts are retrieved from high profilers' followers
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 text-center">
                        <div class="icon-block mb_margin30 ">
                            <span class="icon-color">
                                <i class="fa fa-user-plus"></i>
                            </span>
                            <h3><strong>Step 3</strong></h3>
                            <p>
                                <span class="bolder">Chances of Twitter accounts to be followed back to the company is higher</span><br>
                                Due to common interests from the high profilers and the company</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- End [Features Description] -->

         <!-- Start [features]--> 

        <section id="features">
            <div class="container">
                <div class="row valign-wrapper set-wrapper">
                    <div class="col-md-6">
                        <div class="choose_phone hidden-xs hidden-sm">
                            <img class="img img-responsive phone1" src="assets/images/BO_1.png" alt="Phone1" />
                            <img class="img img-responsive phone2" src="assets/images/crowdie_2.png" alt="Phone1" />
                        </div>
                    </div>
                    <div class="col-md-6 choose_us_txt">
                        <h2 class="mb40">Explore Amazing <span class="bolder">Features</span></h2>
                        
                        
                        <ul class="mt30 pl0">
                        <h3>Business Owner</h3>

                            <li class="amazing-feature-text">
                                <i class="fa  fa-check icon-color"></i><p>Link to Twitter account</p>
                            </li>

                            <li class="amazing-feature-text">
                                <i class="fa fa-check icon-color"></i><p>Choose interest to match with related Twitter's high profilers &amp; followers</p>
                            </li>

                            <li class="amazing-feature-text">
                                <i class="fa fa-check icon-color"></i><p>View dashboard for Crowdie's performance</p>
                            </li>


                        </ul>

                        
                        <ul class="mt30 pl0">
                        <h3>Crowdies</h3>

                            
                            <li class="amazing-feature-text">
                                <i class="fa  fa-check icon-color"></i><p>Choose interest to searh for suitable companies</p>
                            </li>

                            <li class="amazing-feature-text">
                                <i class="fa fa-check icon-color"></i><p>Search for Business Owner account to apply for job</p>
                            </li>

                            <li class="amazing-feature-text">
                                <i class="fa fa-check icon-color"></i><p>Follow/Skip suggested Twitter accounts</p>
                            </li>
                            <li class="amazing-feature-text">
                                <i class="fa fa-check icon-color"></i><p>Earn Titles &amp; Rewards to be converted to cash</p>
                            </li>

                                                     
                        </ul>
                        
                    </div>
                </div>
            </div>   
        </section>

        <!-- End [features]--> 

        <!-- Start [Some Facts]--> 

       <!--  <section id="some_facts">
            <div class="container">
                <div class="row">
                    <h2 class="text-center mb40 mb_margin0">Fact of <span class="bolder">Secret Sparrow</span></h2>
                </div>   
                <div class="row text-center circle-block"> -->
                    <!-- <div class="col-md-3 col-sm-3 wow zoomIn animated mob-circle" data-wow-duration="2s"> 
                        <div id="circle1">
                            <strong>
                                <span class="counter" style="display: inline-block;">5147</span>k                            
                            </strong>
                        </div>
                        <h4 class="circle-txt">Line Of Code</h4>
                        <a href="#"  class=" btn circle-user">
                            <i data-toggle="tooltip" data-placement="bottom" title="Line Of Code" class="fa fa-code"></i>
                        </a>
                    </div>  -->
                   <!--  <div class="col-md-3 col-sm-3 wow zoomIn animated mob-circle" data-wow-duration="2s">
                        <div id="circle2"><strong><span class="counter" style="display: inline-block;">100</span>k</strong></div>
                        <h4 class="circle-txt">Happy User</h4>
                        <a href="#" class="btn circle-user">
                            <i data-toggle="tooltip" data-placement="bottom" title="Happy User" class="fa fa-user"></i>
                        </a>
                    </div> -->
                    <!-- <div class="col-md-12 col-sm-12 wow zoomIn animated mob-circle" data-wow-duration="2s">
                        <div id="circle3"><strong><span class="counter" style="display: inline-block;">1,100</span>+</strong></div>
                        <h4 class="circle-txt">Downloads</h4>
                        <a href="#" class="btn circle-user">
                            <i data-toggle="tooltip" data-placement="bottom" title="Downloads" class="fa fa-cloud-download"></i>
                        </a>
                    </div> -->
                   <!--  <div class="col-md-3 col-sm-3 wow zoomIn animated mob-circle" data-wow-duration="2s">
                        <div id="circle4"><strong><span class="counter" style="display: inline-block;">4.5</span></strong></div>
                        <h4 class="circle-txt">App Rates</h4>
                        <a href="#" class="btn circle-user">
                            <i data-toggle="tooltip" data-placement="bottom" title="App Rates" class="fa fa-star"></i>
                        </a>
                    </div> -->
      <!--           </div>
            </div>
        </section> -->

        <!-- End [Some Facts]--> 
        <!-- Start [Video]-->
        <section id="video" class="video">
        <section class="video-jarallax text-center">
            <div class="container">
                <div class="row">
                    <h2 class="mb40">Introduction <span class="bolder">Video</span></h2>
                        <p class="mb40 col-md-10 col-md-offset-1">Secret Sparrow is a clean, modern landing page for showcase your elegant app along its Video. Showup your elegant app video to your precious users</p>
                       <iframe width="75%" height="480" src="https://www.youtube.com/embed/cjYJnLFy2Ps" frameborder="0" allowfullscreen></iframe>

                    
                </div>
            </div>
        </section>
        </section>
        <!-- End [Video]--> 


        <!-- Start [Screenshots]--> 

        <section id="screenshots" class="screenshot">
            <div class="container text-center mb50">
                <h2><span class="bolder">Business Owner</span> UI Screenshots </h2>
            </div>
            <!--Screenshot Phone Background-->  
            <div class="container">          
            <div class="owl-main-section">
                <div class="ss-phone hidden-xs hidden-sm">
                    <img src="assets/images/iphone.png" alt="iphone schreenshot background">
                </div>
            </div>                                    
            <!-- Screenshots -->
            <div id="owl-demo" class="owl-carousel main-item-list">
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-1.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-2.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-3.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-4.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-5.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-6.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-7.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-8.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-9.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-10.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-11.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-12.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-13.jpg" ></div>
                </div>
                <div class="item">
                    <div class="thumb"> <img class="" src="assets/images/screenshot/sc-14.jpg" ></div>
                </div>

            </div>
            </div>
        </section>

        <!-- End [Screenshots]--> 

        <!--Start [Twitter]-->

       <!--  <section class="twitter-main" id="tweet">
            <div class="container text-center">
                <h2 class="mb40">Tweet @ <span class="bolder">SecretSparrow</span></h2>
            </div>
            <div class="container">
                <div class="tweet text-center"></div>
            </div>
        </section> -->

        <!--End [Twitter]-->
        <!--Start [Subscribe]-->

        <section id="subscribe">
            <div class="container text-center">

                <div class="row">

                    <h2 class="mb40">Sign Up To Our <span class="bolder">Waiting List</span></h2>

                    <p class="col-md-10 col-md-offset-1 text-center title-para mb30">
                        Enter your email address into our subscribe form to be on our waiting list. <br>
                        You can get the link to download our application in your email soon - Subscribe with us.
                    </p>
                  

                <div class="container">
                    <div class="row content" > 
                        <form class="form-material set-submit-input" id="form">
                            <div class="form-group set-submit-box spacing">
                                <input type="text" name="first_name" class="form-control" id="first_name" required="">                                            
                                <span class="form-bar"></span>
                                <label class="float-label" for="first_name">Your name</label>
                            </div>
                            
                            <div class="form-group set-submit-box spacing">

                                <input type="text" class="form-control" name="subscriber_email" id="subscriber_email" required="">

                                <span class="form-bar"></span>
                                <label class="float-label" for="subscriber_email">Email address</label>
                            </div>
                            <div class="form-group set-submit-box spacing">  
                            <button type="button" class="btn btn-primary btn-subscribe common-btn colored-button float-button-light" id="send">Subscribe <i class="fa fa-paper-plane ml5" aria-hidden="true"></i></button>
                            </div> 
                        </form>
                    </div>                       
                </div>
            </div>
            </div>
        </section>

        <!--End [Subscribe]-->
 
        <!--Start [Footer]-->

        <footer>
            <section id="footer">
                <div class="container text-center">
                    <div class="row">

<!--
                        <h2>The Best App to gain <span class="bolder">Twitter</span> followers for your <span class="bolder">Business</span></h2>
                        <div class="col-md-12"> 

                            <p class="mb20 title-para">                                 

                                Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt 
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labori
                                s nisi ut aliquip ex ea commodo consequat.                                                                               
                            </p>
                        </div>
-->
                        <!-- <h2>Spread the words out.</h2> -->
                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <!-- <div class="addthis_inline_share_toolbox"></div>
                        <br>
                        <br> -->
                        <!-- <center> -->
                          <!-- Go to www.addthis.com/dashboard to customize your tools -->
                          <p class="footer-copyright mb0">Spread the words</p>
                          <br>
                          <div class="addthis_inline_share_toolbox"></div>
                        <!-- </center> -->
                        

<!--
                        <div class="mb20">

                            <a class="btn default_color available-btn colored-button float-button-light" href="http://google.com">
                                        <i class="fa fa-android"></i>
                                        <em> Download now </em>
                                        Android apk
                                    </a>                                
                        </div>
-->
                        <!-- <br> -->
                        <br>
                        <p class="footer-copyright mb0"> Secret Sparrow &copy; <span id="copyright-year">2016</span>. All rights reserved </p>
                        <br>
                        <small><a href="/dashboard">Administrator Dashboard</a></small>
                    </div>
                </div>
            </section>

        </footer>

        <!--End [Footer]--> 
         <!-- Move to up -->
        <a class="btn btn-lg waves-effect waves-light scrollup"><i class="fa fa-arrow-up"></i></a>

        <!--Required JS -->
        <!-- <script src="assets/js/jquery.2.2.3.min.js"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>           

        <!--Jarallax JS-->
        <script src="assets/js/jarallax.min.js"></script>

        <!--Sidebar JS-->
        <script src="assets/js/jquery.sidebar.min.js"></script>         

         <!--Background Image-->
        <script src="assets/js/jquery.backstretch.js"></script>          

        <!--OWL Carousel JS -->
        <script src="assets/js/owl.carousel.min.js"></script> 

        

        <!--Video Magnification JS -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>        

        <!-- Count Numbers -->
        <script src="assets/js/jquery.counterup.js"></script>          

        <!-- animation -->
        <script src="assets/js/waypoints.min.js"></script>

        <!--Custom Scrollbar JS-->
        <script src="assets/js/mCustomScrollbar.min.js"></script>

        <!-- Twiiter JS -->
        <script src="assets/js/tweetie.min.js"></script> 
        
        <!--Wave-Effect JS-->                                    
        <script src="assets/js/waves.min.js"></script>

        <!-- Google map -->                       
         
       <!--  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/js/gmaps.js"></script>    -->   
		
        <!--Custom JS-->    
        <script src="assets/js/custom.js"></script>  
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57df6f494b0b7d0c"></script>

        <!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57de5be046a27420"></script> -->


        <!-- <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-84295667-1', 'auto');
          ga('send', 'pageview');

        </script> -->

         <script>
         $.backstretch([
        "assets/images/banner/slide_1.jpg",
        "assets/images/banner/slide_2.jpg",
        "assets/images/banner/slide_3.jpg"

        ], {
            fade: 750,
            duration: 4000
        });

        </script>
        <script>
        addthis.sharecounters.getShareCounts('facebook', function(obj) {        
                        console.log(obj);
                        $.ajax({
                            type:'POST',
                            url:"/social",
                            contentType:'application/json; charset=UTF-8',
                            dataType:'json',
                            data:JSON.stringify({"platform":'facebook','count':obj.count}),
                                success:function(data,status,xhr){
                                    console.log(data);
                                },
                                complete:function(status,xhr){
                                    if(xhr!=='success'){
                                        console.log(xhr);
                                    }
                                }
                            });
                    });
                    addthis.sharecounters.getShareCounts('twitter', function(obj) {        
                        console.log(obj);
                        var x = 1;
                        $.ajax({
                            type:'POST',
                            url:"/social",
                            contentType:'application/json; charset=UTF-8',
                            dataType:'json',
                            data:JSON.stringify({"platform":'twitter','count':obj.count}),
                                success:function(data,status,xhr){
                                    console.log(data);
                                },
                                complete:function(status,xhr){
                                    if(xhr!=='success'){
                                        console.log(xhr);
                                    }
                                }
                            });
                    });
                    addthis.sharecounters.getShareCounts('googleplus', function(obj) {        
                        console.log(obj);
                        $.ajax({
                            type:'POST',
                            url:"/social",
                            contentType:'application/json; charset=UTF-8',
                            dataType:'json',
                            data:JSON.stringify({"platform":'googleplus','count':obj.count}),
                                success:function(data,status,xhr){
                                    console.log(data);
                                },
                                complete:function(status,xhr){
                                    if(xhr!=='success'){
                                        console.log(xhr);
                                    }
                                }
                            });
                    });
            $(document).ready(function(){
                // function testing(){
                    
                
                $(document).on('click','#send',function(){
                    var name=$('#first_name').val();
                    var email=$('#subscriber_email').val();
                    $.ajax({
                        type:'POST',
                        url:"/product/register",
                        contentType:'application/json; charset=UTF-8',
                        dataType:'json',
                        data:JSON.stringify({"email":email,"name":name}),
                            success:function(data,status,xhr){
                                $("#form").hide();
                                $(".content").append('<h1>Registration Complete!</h1>')
                            },
                            complete:function(status,xhr){
                                if(xhr!=='success'){
                                    console.log(xhr);
                                }
                            }
                    });
                });
                // $(document).on('click','.at-follow-btn',function(){
                //     console.log(this);
                //     console.log($(this).attr('data-svc'));
                //     var platform=$(this).attr('data-svc');
                //     if (platform=='facebook') {
                //         // $('#facebook-banner').show();
                //         $.ajax({
                //             type:'POST',
                //             url:"/social",
                //             contentType:'application/json; charset=UTF-8',
                //             dataType:'json',
                //             data:JSON.stringify({"platform":'facebook'}),
                //                 success:function(data,status,xhr){
                //                     console.log(data);
                //                 },
                //                 complete:function(status,xhr){
                //                     if(xhr!=='success'){
                //                         console.log(xhr);
                //                     }
                //                 }
                //             });
                //     }
                //     else if (platform=='youtube') {
                //         // $('#facebook-banner').show();
                //         $.ajax({
                //             type:'POST',
                //             url:"/social",
                //             contentType:'application/json; charset=UTF-8',
                //             dataType:'json',
                //             data:JSON.stringify({"platform":'youtube'}),
                //                 success:function(data,status,xhr){
                //                     console.log(data);
                //                 },
                //                 complete:function(status,xhr){
                //                     if(xhr!=='success'){
                //                         console.log(xhr);
                //                     }
                //                 }
                //             });
                //     }
                //     else{
                //         // $('#facebook-banner').show();
                //         $.ajax({
                //             type:'POST',
                //             url:"/social",
                //             contentType:'application/json; charset=UTF-8',
                //             dataType:'json',
                //             data:JSON.stringify({"platform":'twitter'}),
                //                 success:function(data,status,xhr){
                //                     console.log(data);
                //                 },
                //                 complete:function(status,xhr){
                //                     if(xhr!=='success'){
                //                         console.log(xhr);
                //                     }
                //                 }
                //             });
                //     }

                // });
               
                    
                });
        
        </script>  
    </body>
</html>
